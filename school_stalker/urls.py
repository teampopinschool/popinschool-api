from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.authtoken import views
from .social_auth import FacebookLogin
from .social_auth import GoogleLogin
from .social_auth import TwitterLogin


urlpatterns = [
    url(r'^', include('apps.core.urls', namespace=u"core")),
    url(r'^schools/', include('apps.schools.urls', namespace=u"schools")),
    url(r'^address/', include('apps.adresses.api.urls', namespace=u"adresses")),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/api-token-auth/', views.obtain_auth_token),
    url(r'^reports/', include('apps.reports.urls')),

    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^rest-auth/google/$', GoogleLogin.as_view(), name='google_login'),
    url(r'^rest-auth/twitter/$', TwitterLogin.as_view(), name='twitter_login')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
