import codecs
import os


def get_template(name, data):
    location = os.path.dirname(os.path.realpath(__file__)) + '/' + name + ".html"
    file = codecs.open(location, 'r')
    template = file.read()

    if name == 'forget_password':
        template = template.replace('{{ user_name }}', data['user_name'])
        template = template.replace('{{ link }}', data['link'])
    elif name == 'new_evaluation':
        template = template.replace('{{ user_name }}', data['user_name'])
        template = template.replace('{{ school_name }}', data['school_name'])
        template = template.replace('{{ comment }}', data['comment'])
        template = template.replace('{{ link }}', data['link'])

    return template
