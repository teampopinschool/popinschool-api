from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from rest_auth.views import LoginView
from rest_auth.registration.views import SocialLoginView
from rest_auth.social_serializers import TwitterLoginSerializer
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.adapter import get_adapter as get_account_adapter
from allauth.account.utils import user_username, user_email, user_field, perform_login
from allauth.utils import valid_email_or_none
from django.contrib.auth.models import User
from allauth.exceptions import ImmediateHttpResponse
from rest_framework.response import Response


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class TwitterLogin(LoginView):
    serializer_class = TwitterLoginSerializer
    adapter_class = TwitterOAuthAdapter


class DefaultOverrideAccountAdapter(DefaultSocialAccountAdapter):

    def populate_user(self,
                      request,
                      sociallogin,
                      data,
                      other=None):
        print('populate_user')
        username = data.get('username')
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        email = data.get('email')
        name = data.get('name')
        user = sociallogin.user
        user_username(user, username or data.get('email'))
        user_email(user, valid_email_or_none(email) or '')
        name_parts = (name or '').partition(' ')
        user_field(user, 'first_name', first_name or name_parts[0])
        user_field(user, 'last_name', last_name or name_parts[2])
        return user

    def pre_social_login(self, request, sociallogin):
        try:
            user = User.objects.get(email=sociallogin.user.email)
            if user:
                # sociallogin.is_existing = False
                sociallogin.connect(request, user)
                raise ImmediateHttpResponse(Response('ok'))
        except Exception as e:
            print(e)
            pass

        pass
