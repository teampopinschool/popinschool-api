from django.contrib import admin
from django.shortcuts import HttpResponse

from .models import Profile, Agent
from django.core.files.storage import FileSystemStorage
from django.template.loader import render_to_string
from weasyprint import HTML


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user')
    search_fields = ('user', )

    def render_pdf_view(self, request, query_set):
        file_name = 'contract.pdf'
        HTML('http://localhost:8000/reports/contract/3/').write_pdf('/tmp/' + file_name)
        fs = FileSystemStorage('/tmp')

        with fs.open(file_name) as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="' + file_name + '"'
            return response

    render_pdf_view.short_description = 'Gerar relatório'

    # actions = [render_pdf_view]


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Agent)
