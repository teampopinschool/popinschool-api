import os
import base64
import re

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from versatileimagefield.fields import VersatileImageField
from rest_framework.authtoken.models import Token

from ..utils.countries import COUNTRIES
from ..utils.gender import GENDERS


def get_upload_path(self, filename):
    path = self._upload_to + '/' + filename
    return path


def get_docs_upload_path(self, filename):
    path = self._upload_to + '/docs/' + filename
    return path


class Agent(models.Model):
    name = models.CharField(max_length=20, null=True, blank=True)
    active = models.BooleanField(blank=False)

    def __str__(self):
        return self.name


class Profile(models.Model):

    _upload_to = 'profile'

    EN_LEVEL_CHOICES = (
        ('A0', 'A0 - 100 % Principiante'),
        ('A1', 'A1 - Principiante'),
        ('A2', 'A2 - Principiante avançado'),
        ('B1', 'B1 - Pré - intermediário'),
        ('B2', 'B2 - Intermediário 1'),
        ('C1', 'C1 - Intermediário 2'),
        ('C2', 'C2 - Avançado'),
        ('O', 'Other')
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    nationality = models.CharField(max_length=2, choices=COUNTRIES)
    gender = models.CharField(max_length=1, choices=GENDERS)
    birth_day = models.DateField(null=True, blank=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    whatsapp = models.CharField(max_length=20, null=True, blank=True)
    image = VersatileImageField(upload_to=get_upload_path, null=True, blank=True)
    social_image = models.CharField(max_length=300, null=True, blank=True)
    promo_code = models.CharField(max_length=50, null=True, blank=True)
    indicated_by = models.OneToOneField(User, null=True, blank=True, related_name='indicated_by')
    agent = models.ForeignKey(Agent, null=True, blank=True)

    cpf = models.CharField(max_length=20, null=True, blank=True)
    rg = models.CharField(max_length=20, null=True, blank=True)
    orgao_expeditor = models.CharField(max_length=10, null=True, blank=True)
    street = models.CharField(max_length=100, null=True, blank=True)
    complement = models.CharField(max_length=20, null=True, blank=True)
    neighborhood = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=100, null=True, blank=True)
    country = models.CharField(max_length=2, choices=COUNTRIES, null=True, blank=True)
    cep = models.CharField(max_length=20, null=True, blank=True)
    passport_number = models.CharField(max_length=100, null=True, blank=True)
    passport_validity = models.DateField(null=True, blank=True)
    phone = models.CharField(max_length=18, null=True, blank=True)
    english_level = models.CharField(max_length=2, choices=EN_LEVEL_CHOICES, null=True, blank=True)
    english_level_other = models.CharField(max_length=100, null=True, blank=True)

    # to recover password
    password_token = models.CharField(max_length=70, null=True, blank=True)
    # Docs
    cv = models.FileField(upload_to=get_docs_upload_path, null=True, blank=True)
    passport = models.FileField(upload_to=get_docs_upload_path, null=True, blank=True)
    school_letter = models.FileField(upload_to=get_docs_upload_path, null=True, blank=True)

    def __str__(self):
        return str(self.id) + ' - ' + self.user.first_name

    def save(self, *args, **kwargs):
        # self.username = self.username.lower()
        if not self.promo_code:
            self.promo_code = new_promo_code(self.user.first_name)
            while Profile.objects.filter(promo_code=self.promo_code).exists():
                self.promo_code = new_promo_code(self.user.first_name)

        return super(Profile, self).save(*args, **kwargs)

    def get_cv(self):
        return self.cv.url if self.cv else None


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def new_promo_code(first_name):
    name = first_name.split(' ')[0][:5]
    name = re.sub('[^a-zA-Z0-9 \n\.]', '', name)

    token = ''
    while len(token) <= 2:
        token = os.urandom(2)
        token = re.sub('[^a-zA-Z0-9 \n\.]', '', str(token))

    return name + '-' + token