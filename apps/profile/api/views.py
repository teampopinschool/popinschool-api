from rest_framework.response import Response
from rest_framework import viewsets, permissions, status
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import HttpResponse
from rest_framework.decorators import detail_route, api_view
from rest_framework.views import APIView
import hashlib
from random import randint
from django.core.mail import send_mail
from base64 import b64decode
from django.core.files.base import ContentFile
import uuid

from django.contrib.auth.models import User

from apps.contract.api.serializers import ContractFullDescriptionSerializer
from apps.contract.models import Contract
from apps.schools.api.serializers import AlreadyStudiedHereSerializer
from apps.schools.models import AlreadyStudiedHere
from school_stalker.settings import EMAIL_HOST_USER
from school_stalker.template_email.template import get_template
from ..models import Profile, Agent
from .serializers import ProfileSerializer, AgentSerializer, UserSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    """
    Endpoint to register users Profile. It will create automaticaly an user
    and the respective Profile
    POST params:
        bio: my bio text
        gender:M
        birth_day:2001-10-01
        username:gpzim3
        first_name:greg
        last_name:pacheco
        password:xyxyxyxyd242
        nationality:BR
        email:a@a.com
        city:Dublin
        whatsapp:+56787654567
    Response serializer:
    {
        "bio": "m bio",
        "gender": "M",
        "nationality": "BR",
        "birth_day": "2001-10-01",
        "city":"Dublin",
        "whatsapp":"+56787654567",
        "user": {
            "username": "gpzim3",
            "first_name": "greg",
            "last_name": "pacheco",
            "email": "a@a.com"
        }
    }
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id', 'user__username')
    http_method_names = ['get', 'post', 'update', 'options']

    @detail_route(methods=['GET', 'OPTIONS'], permission_classes=[IsAuthenticated, ])
    def get_profile(self, request, pk=None):
        serializer = UserSerializer(request.user)

        if pk == 'me':
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)


    @detail_route(methods=['POST', 'OPTIONS'], permission_classes=[])
    def send_image(self, request, pk=None):
        if request.user.is_authenticated:
            request.user.profile.social_image = request.data.get('image')
            request.user.profile.save()
            return HttpResponse('Feito!')
        else:
            return HttpResponse('User offline')

    @detail_route(methods=['GET', 'OPTIONS'], permission_classes=[IsAuthenticated, ])
    def get_contracts(self, request, pk=None):
        try:
            profile = request.user.profile
            contracts = Contract.objects.filter(profile=profile).order_by('-id')
            serializer = ContractFullDescriptionSerializer(contracts, many=True)

            data = {'contracts': serializer.data}
            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            data = {'message': 'There was an error during the process: ' + str(e)}
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @detail_route(methods=['GET', 'OPTIONS'], permission_classes=[IsAuthenticated, ])
    def get_comments(self, request, pk=None):
        try:
            profile = request.user.profile
            comments = AlreadyStudiedHere.objects.filter(user=profile).order_by('-id')
            serializer = AlreadyStudiedHereSerializer(comments, many=True)

            data = {'comments': serializer.data}
            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            data = {'message': 'There was an error during the process: ' + str(e)}
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @detail_route(methods=['POST', 'OPTIONS'], permission_classes=[])
    def update_profile(self, request, pk=None):
        try:
            profile_data = request.data
            user_data = profile_data['user']
            user = User.objects.get(pk=profile_data['user_id'])

            if user and user.profile:
                user.first_name = user_data['first_name']
                user.last_name = user_data['last_name']
                user.profile.gender = profile_data['gender']
                user.profile.nationality = profile_data['nationality']
                user.profile.city = profile_data['city']
                user.profile.whatsapp = profile_data['whatsapp']
                user.profile.birth_day = profile_data['birth_day']
                user.profile.cpf = profile_data['cpf']
                user.profile.rg = profile_data['rg']
                user.profile.orgao_expeditor = profile_data['orgao_expeditor']
                user.profile.street = profile_data['street']
                user.profile.complement = profile_data['complement']
                user.profile.neighborhood = profile_data['neighborhood']
                user.profile.city = profile_data['city']
                user.profile.state = profile_data['state']
                user.profile.country = profile_data['country']
                user.profile.cep = profile_data['cep']
                user.profile.passport_number = profile_data['passport_number']
                user.profile.passport_validity = profile_data['passport_validity']
                user.profile.phone = profile_data['phone']
                user.profile.english_level = profile_data['english_level']
                user.profile.english_level_other = profile_data['english_level_other']

                #save image
                if profile_data['image']:
                    user.profile.image = base64ToFile(profile_data['image'])

                # save docs
                try:
                    if request.FILES.get('cv'):
                        cv = request.FILES['cv']
                        user.profile.cv = cv
                    elif 'cv' in request.data and request.data['cv']['value']:
                        cv = base64ToFile(request.data['cv']['value'])
                        user.profile.cv = cv

                    if request.FILES.get('passport'):
                        passport = request.FILES['passport']
                        user.profile.passport = passport
                    elif 'passport' in request.data and request.data['passport']['value']:
                        passport = base64ToFile(request.data['passport']['value'])
                        user.profile.passport = passport

                    if request.FILES.get('school_letter'):
                        school_letter = request.FILES['school_letter']
                        user.profile.school_letter = school_letter
                    elif 'school_letter' in request.data and request.data['school_letter']['value']:
                        school_letter = base64ToFile(request.data['school_letter']['value'])
                        user.profile.school_letter = school_letter

                except Exception as e:
                    print(e)
                    pass

                user.profile.save()
                user.save()

            data = {'message': 'ok'}
            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            data = {'message': 'There was an error during the process: ' + str(e)}
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def forgot_password_get_token(request):
    """
    Endpoint to generate a random token to update password after that
    """

    email = request.query_params['email']
    h = hashlib.sha256()
    h.update(bytes(email + str(randint(0, 1000)), encoding='utf-8'))

    try:
        user = User.objects.get(email=email)
        profile = user.profile
        profile.password_token = h.hexdigest()
        profile.save()

        # send email
        link = 'https://www.popinschool.com/forgot_password/' + profile.password_token
        message = "Don't worry about it, just click in that "+link+" to recover! " \
                  "<br>" \
                  "If you don't forgot your password, just ignore this email"
        data = {'user_name': user.first_name, 'link': link}
        template = get_template('forget_password', data)
        send_mail(
            'Popin - Alteração de senha',
            message,
            EMAIL_HOST_USER,
            [email],
            fail_silently=False,
            html_message=template
        )

    except Exception as e:
        raise
        # data = {'message': 'User not found'}
        # return Response(data, status=status.HTTP_400_BAD_REQUEST)

    return Response('ok', status=status.HTTP_200_OK)


@api_view(['GET'])
def forgot_password_get_avatar_by_token(request):
    token = request.query_params['token']
    if not token:
        data = {'message': 'Token is required'}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

    profile = Profile.objects.get(password_token=token)

    if not profile or not profile.user:
        data = {'message': 'User not found'}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

    if profile.image:
        data = {'image': profile.image}
        return Response(data, status=status.HTTP_200_OK)

    if profile.social_image:
        data = {'image': profile.social_image}
        return Response(data, status=status.HTTP_200_OK)

    data = {'message': 'Image not found'}
    return Response(data, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def forgot_password_update(request):
    """
    Endpoint to update password based on token
    """
    token = request.data['token']
    password = request.data['password']

    if not token:
        data = {'message': 'Token is required'}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

    if not password:
        data = {'message': 'Password is required'}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

    profile = Profile.objects.get(password_token=token)

    if not profile or not profile.user:
        data = {'message': 'User not found'}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

    try:
        user = profile.user
        user.set_password(password)
        user.save()
        return Response('ok', status=status.HTTP_200_OK)

    except Exception as e:
        data = {'message': 'There was an error during the process: ' + str(e)}
        return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def base64ToFile(data):
    format, imgstr = data.split(';base64,')
    ext = format.split('/')[-1]
    name = str(uuid.uuid4()) + '.' + ext
    avatar = ContentFile(b64decode(imgstr), name=name)

    return avatar


class AgentViewSet(viewsets.ModelViewSet):
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer
    filter_fields = ('pk', 'name', 'active')
    http_method_names = ['get', 'post', 'update', 'options']