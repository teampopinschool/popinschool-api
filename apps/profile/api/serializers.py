from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ..models import Profile, Agent


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class ProfileSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id',)


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(write_only=True)
    email = serializers.EmailField(write_only=True)
    first_name = serializers.CharField(write_only=True)
    last_name = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)
    user = UserSerializer(read_only=True)
    favorite_schools = serializers.SerializerMethodField()

    cv = serializers.SerializerMethodField()
    passport = serializers.SerializerMethodField()
    school_letter = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ('id', 'user_id', 'image',
                  'bio', 'gender', 'nationality', 'birth_day', 'username', 'email',
                  'first_name', 'last_name', 'password', 'user', 'city', 'whatsapp',
                  'social_image', 'promo_code', 'cv', 'passport', 'school_letter',
                  'cpf', 'rg', 'orgao_expeditor', 'street', 'complement',
                  'neighborhood', 'city', 'state', 'country', 'cep',
                  'passport_number', 'passport_validity', 'phone', 'english_level',
                  'english_level_other', 'favorite_schools')

        write_only_fields = ('password',)

    def get_cv(self, profile):
        try:
            if profile.cv:
                return profile.cv.url
        except Exception as e:
            pass

        return ''

    def get_passport(self, profile):
        try:
            if profile.passport:
                return profile.passport.url
        except Exception as e:
            pass

        return ''

    def get_school_letter(self, profile):
        try:
            if profile.school_letter:
                return profile.school_letter.url
        except Exception as e:
            pass

        return ''

    def get_favorite_schools(self, profile):
        return [school.pk for school in profile.favorite_schools.all()]

    @transaction.atomic()
    def create(self, validated_data):
        if User.objects.filter(email=validated_data['email']).exists():
            raise ValidationError(
                {'email': ['User with this email already exist']})

        if User.objects.filter(username=validated_data['username']).exists():
            raise ValidationError(
                {'username': ['User with this username already exist']})

        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()

        user.profile.bio = validated_data.get('bio')
        user.profile.city = validated_data.get('city')
        user.profile.gender = validated_data.get('gender')
        user.profile.nationality = validated_data.get('nationality')
        user.profile.birth_day = validated_data.get('birth_day')

        if validated_data.get('promo_code'):
            promo_code = validated_data.get('promo_code')
            parent_user = User.objects.filter(profile__promo_code=promo_code).first()
            if parent_user:
                user.profile.indicated_by = parent_user

        user.profile.save()

        return user.profile


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    new_password = serializers.CharField(required=True)
    user = serializers.EmailField(required=True)


class AgentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agent
        fields = ('pk', 'name', 'active')
