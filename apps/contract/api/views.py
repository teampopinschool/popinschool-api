from django.shortcuts import HttpResponse
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated

from apps.contract.api.serializers import ContractSerializer
from apps.contract.models import Contract
from weasyprint import HTML
from django.core.files.storage import FileSystemStorage


class ContractViewSet(viewsets.ModelViewSet):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    http_method_names = ['get', 'post', 'update', 'options']

    @detail_route(methods=['get'], permission_classes=[IsAuthenticated, ])
    def budget(self, request, pk=None):
        try:
            file_name = 'budget.pdf'
            url = 'http://' + request.get_host() + '/reports/contract/' + pk + '/budget/'
            print(url)
            HTML(url).write_pdf('/tmp/' + file_name)
            fs = FileSystemStorage('/tmp')

            with fs.open(file_name) as pdf:
                response = HttpResponse(pdf, content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="' + file_name + '"'
                return response
        except Exception as e:
            raise e
