from rest_framework import serializers
from apps.contract.models import Contract
from apps.schools.api.serializers import SimpleSchoolSerializer, CourseShiftsSerializer, CoursePeriodSerializer


class ContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = '__all__'


class ContractFullDescriptionSerializer(serializers.ModelSerializer):
    course_name = serializers.SerializerMethodField()
    school_name = serializers.SerializerMethodField()

    period = CoursePeriodSerializer()
    shift = CourseShiftsSerializer()

    class Meta:
        model = Contract
        fields = '__all__'

    def get_course_name(self, contract):
        return contract.period.course.type.name

    def get_school_name(self, contract):
        return contract.school.name