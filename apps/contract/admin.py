from django.contrib import admin
from apps.contract.models import Contract


class ContractAdmin(admin.ModelAdmin):
    list_display = ('profile', 'school', 'shift', 'total', 'created_at')
    search_fields = ('profile', 'school', 'created_at')
    list_filter = ('school', 'created_at')


admin.site.register(Contract, ContractAdmin)
