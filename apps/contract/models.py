from django.db import models

from apps.profile.models import Profile
from apps.schools.models import School, CourseShift, CoursePeriod


class Contract(models.Model):
    total = models.DecimalField(max_digits=6, decimal_places=2,
                                help_text='Price', null=True,
                                blank=True, default=0)
    school = models.ForeignKey(School, related_name='contracts')
    period = models.ForeignKey(CoursePeriod, related_name='contracts')
    shift = models.ForeignKey(CourseShift, related_name='contracts')
    profile = models.ForeignKey(Profile, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField(null=True, blank=False)
    more_info = models.TextField(max_length=500, blank=True)
    tax_book = models.DecimalField(max_digits=6,
                                   decimal_places=2,
                                   help_text='Price',
                                   null=True,
                                   blank=True,
                                   default=0)

    tax_registration = models.DecimalField(max_digits=6,
                                           decimal_places=2,
                                           help_text='Registration',
                                           null=True,
                                           blank=True,
                                           default=0)

    tax_insurance_gov = models.DecimalField(max_digits=6,
                                            decimal_places=2,
                                            help_text='Insurance GOV/GTA',
                                            null=True,
                                            blank=True,
                                            default=0)

    tax_ad_fees = models.DecimalField(max_digits=6,
                                      decimal_places=2,
                                      help_text='Ad. Fees',
                                      null=True,
                                      blank=True,
                                      default=0)

    tax_transfer = models.DecimalField(max_digits=6,
                                       decimal_places=2,
                                       help_text='Transfer',
                                       null=True,
                                       blank=True,
                                       default=0)

    tax_exam_fee = models.DecimalField(max_digits=6,
                                       decimal_places=2,
                                       help_text='Exam fee',
                                       null=True,
                                       blank=True,
                                       default=0)

    shift_price = models.DecimalField(max_digits=6,
                                      decimal_places=2,
                                      help_text='Shift price',
                                      null=True)

    shift_description = models.CharField(max_length=20,
                                         choices=CourseShift.SHIFTS,
                                         null=True)

    def __str__(self):
        return self.profile.user.get_full_name() + ' - ' + self.school.name

    def save(self, *args, **kwargs):
        self.update_values()
        super(Contract, self).save(*args, **kwargs)

    def update_values(self):
        self.tax_book = self.period.tax_book
        self.tax_registration = self.period.tax_registration
        self.tax_insurance_gov = self.period.tax_insurance_gov
        self.tax_ad_fees = self.period.tax_ad_fees
        self.tax_transfer = self.period.tax_transfer
        self.tax_exam_fee = self.period.tax_exam_fee
        self.shift_price = self.shift.price
        self.shift_description = self.shift.shift
        self.total = self.period.get_total_tax() + self.shift.price

    def get_total(self):
        return self.tax_book + \
               self.tax_registration + \
               self.tax_insurance_gov + \
               self.tax_ad_fees + \
               self.tax_transfer + \
               self.tax_exam_fee + \
               self.shift_price
