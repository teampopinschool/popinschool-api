from django.conf.urls import url

from .views import  PeopleViewSet

urlpatterns = [
    url(r'^$', PeopleViewSet.as_view, name='address'),
]
