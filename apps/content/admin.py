from django.contrib import admin
from .models import AboutUs, TermsAndConditions


class AboutUsAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'lang')
    list_filter = ('lang', )


class TermsAndConditionsAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'lang')
    list_filter = ('lang', )


admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(TermsAndConditions, TermsAndConditionsAdmin)
