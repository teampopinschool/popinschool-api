from django.db import models
from ckeditor.fields import RichTextField


class AboutUs(models.Model):

    class Meta:
        verbose_name = 'Sobre nós'
        verbose_name_plural = 'Sobre nós'

    LANG_CHOICES = (
        ('en', 'English'),
        ('es', 'Spanish'),
        ('pt_BR', 'Português - BR')
    )

    text = RichTextField(null=False, blank=False)
    lang = models.CharField(max_length=5, default='en', choices=LANG_CHOICES)

    def __str__(self):
        return 'Sobre nós'


class TermsAndConditions(models.Model):

    class Meta:
        verbose_name = 'Termos e Condições'
        verbose_name_plural = 'Termos e Condições'

    LANG_CHOICES = (
        ('en', 'English'),
        ('es', 'Spanish'),
        ('pt_BR', 'Português - BR')
    )

    text = RichTextField(null=False, blank=False)
    lang = models.CharField(max_length=5, default='en', choices=LANG_CHOICES)

    def __str__(self):
        return 'Sobre nós'