from rest_framework import serializers
from ..models import AboutUs, TermsAndConditions


class AboutUsSerializer(serializers.ModelSerializer):

    class Meta:
        model = AboutUs
        fields = '__all__'


class TermsAndConditionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = TermsAndConditions
        fields = '__all__'
