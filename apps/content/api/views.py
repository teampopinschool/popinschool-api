from rest_framework.viewsets import ModelViewSet
from ..models import AboutUs, TermsAndConditions
from .serializers import AboutUsSerializer, TermsAndConditionsSerializer


class AboutUsViewSet(ModelViewSet):
    queryset = AboutUs.objects.all()
    serializer_class = AboutUsSerializer

    def get_queryset(self):
        queryset = AboutUs.objects.all()

        # Filtering by language
        language = self.request.GET.get('lang')
        if language:
            queryset = queryset.filter(lang=language)

        return queryset


class TermsAndConditionsViewSet(ModelViewSet):
    queryset = AboutUs.objects.all()
    serializer_class = TermsAndConditionsSerializer

    def get_queryset(self):
        queryset = TermsAndConditions.objects.all()

        # Filtering by language
        language = self.request.GET.get('lang')
        if language:
            queryset = queryset.filter(lang=language)

        return queryset