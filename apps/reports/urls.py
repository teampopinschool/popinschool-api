from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^contract/(?P<pk>\d+)/$', views.report_contract),
    url(r'^contract/(?P<pk>\d+)/budget/$', views.report_budget_by_contract),

    url(r'budget/(?P<school_pk>\d+)/(?P<period_pk>\d+)/(?P<shift_pk>\d+)/(?P<start_date>[\w\-]+)/$',
        views.report_budget, name='report_budget_url'),
    url(r'budget/(?P<school_pk>\d+)/(?P<period_pk>\d+)/(?P<shift_pk>\d+)/(?P<start_date>[\w\-]+)/pdf/$',
        views.report_budget_pdf),
]
