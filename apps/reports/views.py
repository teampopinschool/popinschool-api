from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from weasyprint import HTML

from apps.contract.models import Contract
import datetime

from apps.schools.models import School, CoursePeriod, CourseShift


def report_contract(request, pk=None):
    template = loader.get_template('reports/report2.html')
    context = {}
    return HttpResponse(template.render(context, request))


def report_budget_by_contract(request, pk=None):
    template = loader.get_template('reports/budget.html')
    context = {}
    contract = get_object_or_404(Contract, pk=pk)
    context['contract'] = contract
    context['school'] = contract.school
    context['period'] = contract.period
    context['shift'] = contract.shift
    context['course'] = contract.period.course
    context['user_name'] = contract.profile.user.get_full_name()

    return HttpResponse(template.render(context, request))


def report_budget(request, school_pk, period_pk, shift_pk, start_date):
    template = loader.get_template('reports/budget.html')
    school = get_object_or_404(School, pk=school_pk)
    period = get_object_or_404(CoursePeriod, pk=period_pk)
    shift = get_object_or_404(CourseShift, pk=shift_pk)
    contract = Contract(school=school,
                        period=period,
                        shift=shift,
                        created_at=datetime.datetime.now(),
                        start_date=start_date)

    contract.update_values()
    context = {
        'contract': contract,
        'school': school,
        'period': period,
        'shift': shift,
        'course': period.course
    }

    return HttpResponse(template.render(context, request))


def report_budget_pdf(request, school_pk, period_pk, shift_pk, start_date):
    try:
        file_name = 'budget.pdf'

        url = reverse(
            'report_budget_url',
            args=(
                school_pk,
                period_pk,
                shift_pk,
                start_date,
            )
        )

        url = 'http://' + request.get_host() + url
        HTML(url).write_pdf('/tmp/' + file_name)
        fs = FileSystemStorage('/tmp')

        with fs.open(file_name) as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="' + file_name + '"'
            return response

    except Exception as e:
        raise e
