RATINGS_CATEGORIES = (
    ("activities", "Atividades extras"),
    ("methodology", "Metodologia"),
    ("structure", "Estrutura"),
    ("benefity", "Custo Beneficio"),
    ("mix", "Mix de nacionalidade"),
    ("localization", "Localizacao"),
    ("staff", "Atendimento")
)
