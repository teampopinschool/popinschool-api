from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=50)
    subject = models.CharField(max_length=150)
    message = models.TextField()
    email = models.EmailField()

    def __str__(self):
        return self.name
