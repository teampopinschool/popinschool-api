from rest_framework.viewsets import ModelViewSet
from .serializer import ContactSerializer


class ContactViewSet(ModelViewSet):
    """
    Class to send contacts from the site to the API
    JUST POST
    http://localhost:8000/api/contacts/
    {
        "name": "Gregory",
        "email": "t@t.com",
        "subject": "tt",
        "message": "ttttt4564563457777"
    }
    """
    queryset = []
    serializer_class = ContactSerializer

