from django.views.generic import TemplateView
from apps.schools.models import School, Rating, Profile
from .script import import_schools
from django.shortcuts import HttpResponse


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['schools'] = School.objects.all().count()
        context['users'] = Profile.objects.all().count()
        context['ratings'] = Rating.objects.all().count()
        return context


class AboutUs(TemplateView):
    template_name = 'aboutus.html'


def do_import_schools(request):
    import_schools()
    return HttpResponse('Feito!')
