from django.conf.urls import url, include
from rest_framework import routers

from .views import HomeView, AboutUs, do_import_schools
from ..schools.api.view import SchoolViewSet
from ..schools.api.view import last_comments, best_rated_schools, featured_schools
from ..adresses.api.views import AddressViewSet
from ..profile.api.views import ProfileViewSet, forgot_password_get_token, forgot_password_get_avatar_by_token, \
    forgot_password_update, AgentViewSet
from ..contract.api.views import ContractViewSet
from ..contact.api.views import ContactViewSet
from ..content.api.views import AboutUsViewSet, TermsAndConditionsViewSet

from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Pastebin API')

router = routers.DefaultRouter()

router.register(r'schools', SchoolViewSet)
router.register(r'profile', ProfileViewSet)
router.register(r'contract', ContractViewSet)
router.register(r'agent', AgentViewSet)
router.register(r'addresses', AddressViewSet)
router.register(r'contacts', ContactViewSet, base_name='contacts')
router.register(r'content/about-us', AboutUsViewSet, base_name='content_about_us')
router.register(r'content/terms-and-conditions', TermsAndConditionsViewSet, base_name='content_terms')

urlpatterns = [
    url(r'^$', HomeView.as_view(), name="homepage"),
    url(r'^about-us/$', AboutUs.as_view(), name="about-us"),
    url(r'^import_schools/$', do_import_schools, name="import_school"),
    url(r'^api/', include(router.urls)),
    url(r'last_comments/$', last_comments),
    url(r'best_rated_schools/$', best_rated_schools),
    url(r'featured_schools/$', featured_schools),
    url(r'password/get_token', forgot_password_get_token),
    url(r'password/forgot_password_get_avatar_by_token', forgot_password_get_avatar_by_token),
    url(r'password/forgot_password_update', forgot_password_update),
    url(r'^swagger/$', schema_view)
]


