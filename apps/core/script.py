import os
from openpyxl import load_workbook

from apps.adresses.models import (
    State,
    City,
    Region,
    Country
)

from apps.schools.models import (
    School
)

workpath = os.path.dirname(os.path.abspath(__file__))


COUNTRY_ID = 3
def import_schools():
    # l = ['Dublin.xlsx', 'Limerick.xlsx', 'Cork.xlsx', 'Galway.xlsx']
    l = ['Malta.xlsx']

    for c in l:
        wb = load_workbook(filename=workpath + '/' + c, read_only=True)
        ws = wb['Sheet1']

        # D row[3] Simple address - N
        # E row[4] City - ok
        # F row[5] State or Regiao - ok
        # G row[6] Full addres - ok

        for row in ws.rows:
            if School.objects.filter(google_api_code=row[0].value):
                continue

            regions = Region.objects.filter(name=row[5].value)
            if regions:
                region = regions[0]
            else:
                region = Region.objects.create(name=row[5].value)

            state = State.objects.get_or_create(
                name=row[5].value, country_id=COUNTRY_ID, region=region)
            city = City.objects.get_or_create(name=row[4].value, state=state[0])

            School.objects.create(
                google_api_code=row[0].value, name=row[1].value,
                coords=row[2].value, address=row[6].value, city=city[0],
                phone=row[7].value, internationa_phone=row[8].value,
                website=row[9].value, embeded_map=row[10].value,
                work_time=row[11].value
            )
            print(row)
