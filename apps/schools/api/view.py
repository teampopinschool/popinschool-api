from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import api_view, action
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail

from school_stalker.template_email.template import get_template
from ..models import School, Rating, AlreadyStudiedHere
from apps.profile.models import Profile
from .serializers import SchoolSerializer, CommentsSerializer, SimpleSchoolSerializer
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from school_stalker.settings import EMAIL_HOST_USER


class SchoolViewSet(ModelViewSet):
    """
    All text searchs are case insensitive
    Search school by name > school.name
    check if name contains the param name
    http://localhost:8000/api/schools/?name=Escola%20y

    Search school by language > school.language.description
    check if language.description contains the param language
    http://localhost:8000/api/schools/?language=english
    http://localhost:8000/api/schools/?name=Escola&language=english

    Seach school by school.city.name
    check if school.city.name contains the param city
    http://localhost:8000/api/schools/?name=Escola&city=Cidade

    Search schools by schoo.city.country.name
    check if the school country name contains the param country
    http://localhost:8000/api/schools/?name=Escola&city=Cidade&country=canada

    Search schools by school.rating
    check if the school country name contains the param country
    http://localhost:8000/api/schools/?name=Escola&min_rating=1&max_rating=5

    Pagination 10 schools per page
    http://localhost:8000/api/schools/?page=2
    """
    queryset = School.objects.all()
    serializer_class = SchoolSerializer
    # lookup_field = 'slug'

    def get_queryset(self):
        """
        nome - ok, pais - ok, cidade - ok, idiomas - ok, rating - ok
        enableds: min price, max price, class type (indiv, group), shift (turno)
        :return:
        """
        id = self.request.GET.get('id')
        if id:
            return School.objects.filter(id=id)

        queryset = School.objects.all()

        # Filtering by language
        language = self.request.GET.get('language')
        if language:
            queryset = queryset.filter(
                language__description__icontains=language)

        # Filtering by name
        name = self.request.GET.get('name')
        if name:
            queryset = queryset.filter(name__icontains=name)

        # Filtering by city
        city = self.request.GET.get('city')
        if city:
            queryset = queryset.filter(city__name__icontains=city)

        # Filtering by country
        country = self.request.GET.get('country')
        if country:
            queryset = queryset.filter(
                city__state__country__name__icontains=country).distinct('id')

        # Filtering by Rating range
        max = self.request.GET.get('max_rating')
        min = self.request.GET.get('min_rating')

        if max and min:
            # rating_range = range(int(min), int(max)+1)
            max = int(max) + 1
            min = int(min)
            shools_list = [school.id for school in queryset if min <= school.ratings < max]
            queryset = School.objects.filter(id__in=list(set(shools_list)))

        queryset = sorted(queryset, key=lambda t: t.ratings, reverse=True)
        # Filtering by course price
        # min_price = self.request.GET.get('min_price')
        # max_price = self.request.GET.get('max_price')
        # if max_price and min_price:
        #     price_range = range(int(min_price), int(max_price)+1)
        #     queryset = queryset.filter(course__price__in=price_range)
        #
        # # Filtering by visa needed
        # visa = True if self.request.GET.get('visa') == 'true' else False
        # if visa:
        #     queryset = queryset.filter(course__visa_needed=visa)

        # Filtering by course type
        # class_type = self.request.GET.get('classes_type')
        # if class_type:
        #     queryset = queryset.filter(course__classes_type=class_type)

        # Filtering by shift
        # shift = self.request.GET.get('shift')
        # if shift:
        #     queryset = queryset.filter(course__shift=shift)
        #
        return queryset

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated, ])
    def already_studied_here(self, request, pk=None):
        """
        Method to enroll and unroll the logged student (by the token) <br>
        :param unroll: Boolean, Optional to unroll the logged user <br>
        :return: 200 or 400 status and the message
        """
        user = request.user
        profile = Profile.objects.get(user=user)
        school = School.objects.get(pk=pk)
        try:
            if request.data.get('unroll'):
                school.unroll_student(profile)
                data = {'message': 'Student successfull unnroled'}
                return Response(data, status=status.HTTP_200_OK)
            else:
                already_studied_here = AlreadyStudiedHere(
                    user=profile, school=school, data=request.data.get('data'),
                    comment=request.data.get('comment')
                )
                school.new_student(already_studied_here)
                data = {'message': 'Student successfull enroled'}

                # send mail to user
                try:
                    link = 'https://www.popinschool.com/school/' + str(school.id)
                    temp_data = {'user_name': user.first_name,
                                 'school_name': school.name,
                                 'comment': already_studied_here.comment,
                                 'link': link}
                    template = get_template('new_evaluation', temp_data)
                    send_mail(
                        'Popin - Avaliação publicada',
                        '',
                        EMAIL_HOST_USER,
                        [user.email],
                        fail_silently=False,
                        html_message=template
                    )
                except:
                    pass

                return Response(data, status=status.HTTP_200_OK)
        except Exception as e:
            if str(e) == 'Student already enrolled in this school':
                data = {'message': str(e)}
                return Response(data, status=status.HTTP_401_UNAUTHORIZED)
            else:
                data = {
                    'message': 'There was an error during the process: '+str(e)
                }
                return Response(data, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['POST', 'OPTIONS'], permission_classes=[IsAuthenticated, ])
    def update_comment(self, request, pk=None):
        """
        Endpoint to update already_studied_here comment.
        """
        try:
            ash_data = request.data
            ash = AlreadyStudiedHere.objects.get(pk=ash_data['id'])
            ash.comment = ash_data['comment']
            ash.save()

            data = {'message': 'ok'}
            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            data = {'message': 'There was an error during the process: ' + str(e)}
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated, ])
    def ratings(self, request, pk=None):
        """
        Method to post a rating into a school <br>
        /api/schools/464/ratings/ [POST]<br>
        :param comment: text, required  | starts: integer, required |
            type_of_rating: string, required <br>
        Validations:
            - Rating is unique together (User + school + type_of_rating)
            - The student should be enrolled into the school
        """
        school = School.objects.get(pk=pk)
        profile = Profile.objects.get(user=request.user)

        try:
            type_of_rating = request.data['type_of_rating']
            stars = request.data['stars']
            comment = request.data['comment']
            rating = Rating(
                type_of_rating=type_of_rating, stars=int(stars),
                comment=comment)
            school.new_rating(student=profile, rating=rating)
            data = {'message': 'Rating successful posted'}
            return Response(data, status=status.HTTP_200_OK)
        except Exception as e:
            data = {'message': 'There was an error during the process: '+str(e)}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated, ])
    def comment(self, request, pk=None):
        """
        Method to post a comment into a school <br>
        /api/schools/464/comment/ [POST]<br>
        :param comment: text, required  <br>
        :return: 200 or 400 status and the message
        """
        profile = Profile.objects.get(user=request.user)
        school = School.objects.get(pk=pk)
        try:
            comment = request.data['comment']
            school.new_comment(student=profile, comment=comment)
            data = {'message': 'Comment successful posted'}
            return Response(data, status=status.HTTP_200_OK)
        except Exception as e:
            data = {'message': 'There was an error during the process: '+str(e)}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated, ])
    def set_favorite(self, request, pk=None):
        try:
            school = School.objects.get(pk=pk)
            profile = Profile.objects.get(user=request.user)
            profile.favorite_schools.add(school)
            profile.save()

            data = {'message': 'ok'}
            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            data = {'message': 'There was an error during the process: ' + str(e)}
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        pass

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated, ])
    def remove_favorite(self, request, pk=None):
        try:
            school = School.objects.get(pk=pk)
            profile = Profile.objects.get(user=request.user)
            profile.favorite_schools.remove(school)
            profile.save()

            data = {'message': 'ok'}
            return Response(data, status=status.HTTP_200_OK)

        except Exception as e:
            data = {'message': 'There was an error during the process: ' + str(e)}
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        pass


@api_view(['GET'])
def last_comments(request):
    # Fabio decidiu mostrar apenas avaliações com mais de 3 estrelas
    min_rate = '3'
    max = 10

    from decimal import Decimal
    comments = AlreadyStudiedHere.objects.filter(average__gt=Decimal(min_rate))\
                                         .exclude(comment__isnull=True)\
                                         .exclude(comment__exact='')\
                                         .order_by('-id')[:max]
    serializer = CommentsSerializer(comments, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


# @api_view(['GET'])
# def get_by_slug(request):
#     # comments = AlreadyStudiedHere.objects.filter().exclude(comment__isnull=True).exclude(comment__exact='').order_by('-id')[:10]
#     school = School.objects.filter(slug='escola-dr-victor')
#     serializer = SchoolSerializer(school)
#     return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def best_rated_schools(request):
    list = School.objects.filter()[:10]
    list = sorted(list, key=lambda t: t.ratings, reverse=True)
    serializer = SimpleSchoolSerializer(list, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def featured_schools(request):
    list = School.objects.filter(highlighted=True)[:4]
    serializer = SimpleSchoolSerializer(list, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)