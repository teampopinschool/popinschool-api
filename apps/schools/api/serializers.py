from apps.adresses.models import City
from ..models import School, Comment, AlreadyStudiedHere, Rating, CoursePeriod, CourseShift, Course, SchoolCertificate, \
    ImageGalery, Facilities
from apps.profile.api.serializers import ProfileSerializer, \
    ProfileSimpleSerializer

from rest_framework import serializers


class CommentSerializer(serializers.ModelSerializer):
    user = ProfileSerializer()

    class Meta:
        model = Comment
        fields = ['comment', 'date', 'user']


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = ('type_of_rating', 'stars', 'comment')


class CourseShiftsSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField()

    class Meta:
        model = CourseShift
        # fields = ['price', 'shift']
        fields = '__all__'

    def get_total(self, obj):
        return obj.get_total()


class CoursePeriodSerializer(serializers.ModelSerializer):
    shifts = CourseShiftsSerializer(many=True)

    class Meta:
        model = CoursePeriod
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    periods = CoursePeriodSerializer(many=True)
    type = serializers.SerializerMethodField()

    class Meta:
        model = Course
        fields = '__all__'

    def get_type(self, obj):
        if getattr(obj, 'type'):
            return obj.type.name
        else:
            return ''


class SimpleSchoolSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    language = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()
    comments = CommentSerializer(many=True)

    class Meta:
        model = School
        exclude = ['logo_image', 'short_phrase']

    def get_logo(self, obj):
        return obj.get_image()

    def get_rating(self, obj):
        return obj.ratings

    def get_language(self, obj):
        if hasattr(obj.language, 'description'):
            return obj.language.description
        else:
            return ''

    def get_city(self, obj):
        if obj.city:
            res = obj.city.name
            if obj.city.state and obj.city.state.country:
                res += ', ' + obj.city.state.country.name
            return res
        else:
            return ''


class SchoolCertificateSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = SchoolCertificate
        fields = '__all__'

    def get_image(self, obj):
        return obj.get_image()


class FacilitiesSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Facilities
        fields = ('name', 'image', 'icon_text')

    def get_image(self, obj):
        return obj.image.url if obj.image else ''


class ImageGalerySerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = ImageGalery
        fields = ('image', )

    def get_image(self, obj):
        return obj.get_image()


class AlreadyStudiedHereSerializer(serializers.ModelSerializer):
    user = ProfileSerializer()
    ratings = RatingSerializer(many=True)
    school = SimpleSchoolSerializer()

    class Meta:
        model = AlreadyStudiedHere
        fields = ('id', 'user', 'data', 'school', 'comment', 'ratings', 'created_on')


class SchoolSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    rating = serializers.SerializerMethodField()
    language = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()
    comments = CommentSerializer(many=True)
    certificates = SchoolCertificateSerializer(many=True)
    facilities = FacilitiesSerializer(many=True)
    students_enrolled = ProfileSimpleSerializer(many=True)
    ratings_detail = AlreadyStudiedHereSerializer(many=True)
    courses = CourseSerializer(many=True)
    gallery = ImageGalerySerializer(many=True)
    full_rating = serializers.DictField()

    class Meta:
        model = School
        exclude = ['logo_image', 'favorite_users']
        extra_field_kwargs = {'url': {'lookup_field': 'slug'}}

    def get_logo(self, obj):
        return obj.get_image()

    def get_rating(self, obj):
        return obj.ratings

    def get_language(self, obj):
        if hasattr(obj.language, 'description'):
            return obj.language.description
        else:
            return ''

    def get_city(self, obj):
        return obj.get_full_address()


class CommentsSerializer(AlreadyStudiedHereSerializer):
    school = SchoolSerializer()

    class Meta:
        model = AlreadyStudiedHere
        fields = ('user', 'data', 'comment', 'ratings', 'school', 'average', 'created_on')

