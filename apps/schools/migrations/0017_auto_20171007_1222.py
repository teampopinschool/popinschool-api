# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-07 12:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0016_auto_20170923_2135'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Ratings',
            new_name='Rating',
        ),
    ]
