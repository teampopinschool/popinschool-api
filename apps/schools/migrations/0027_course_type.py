# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-07-25 17:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0026_remove_course_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='courses', to='schools.CourseType'),
        ),
    ]
