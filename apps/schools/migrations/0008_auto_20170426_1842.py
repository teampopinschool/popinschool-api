# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-26 18:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0007_auto_20170422_1208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='school',
            name='address',
            field=models.ForeignKey(blank=True, help_text='Informe o endereço da escola', null=True, on_delete=django.db.models.deletion.CASCADE, to='adresses.Address'),
        ),
        migrations.AlterField(
            model_name='school',
            name='admin_email',
            field=models.EmailField(blank=True, help_text='Email de contato da escola', max_length=100, null=True, unique=True, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='school',
            name='embeded_map',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='school',
            name='facebook_page',
            field=models.CharField(blank=True, help_text='Facebook Page', max_length=255, null=True, verbose_name='Facebook Page'),
        ),
        migrations.AlterField(
            model_name='school',
            name='instagram',
            field=models.CharField(blank=True, help_text='Instagram profile', max_length=255, null=True, verbose_name='Instagram'),
        ),
        migrations.AlterField(
            model_name='school',
            name='language',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='schools.Languages'),
        ),
        migrations.AlterField(
            model_name='school',
            name='phone',
            field=models.CharField(blank=True, help_text='Ex.: +55 11 91001 0101', max_length=18, null=True, verbose_name='telefone'),
        ),
        migrations.AlterField(
            model_name='school',
            name='website',
            field=models.CharField(blank=True, help_text='Your website', max_length=255, null=True, verbose_name='WebSite'),
        ),
        migrations.AlterField(
            model_name='school',
            name='youtube_channel_link',
            field=models.CharField(blank=True, help_text='Set the link for yout youtube channel or video', max_length=255, null=True, verbose_name='Youtube Channel'),
        ),
    ]
