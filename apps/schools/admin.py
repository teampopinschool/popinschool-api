from django.contrib import admin
from django.utils.text import slugify
from .models import (
    School,
    ImageGalery,
    Videos,
    EventsOffered,
    Accommodation,
    Languages,
    Rating,
    CoursePeriod,
    Comment,
    AlreadyStudiedHere,
    Promotion,
    CourseShift, Course, SchoolCertificate, CourseType, Facilities)


class ImageInline(admin.TabularInline):
    model = ImageGalery
    extra = 1


class VideoInline(admin.TabularInline):
    model = Videos
    extra = 1


class SchoolAdmin(admin.ModelAdmin):
    inlines = (ImageInline, VideoInline)
    list_display = (
        'id', 'name', 'ratings', 'get_city', 'get_country', 'language', 'show_budget')
    list_filter = ('id', 'name', 'show_budget')
    ordering = ('id', 'name')
    search_fields = ('id', 'name', 'city__name', 'city__state__country__name')
    filter_horizontal = []

    def get_city(self, obj):
        if not obj.city:
            return ''
        return obj.city.name

    get_city.short_description = 'City'
    get_city.admin_order_field = 'city__name'

    def get_country(self, obj):
        if not obj.city:
            return ''
        return obj.city.state.country.name

    get_country.short_description = 'Country'
    get_country.admin_order_field = 'city__state__country__name'

    def set_slug(self, request, queryset):
        for school in queryset:
            if not school.slug:
                school.slug = slugify(school.name)
                school.save()

    set_slug.short_description = 'Definir Slug'
    actions = [set_slug]


class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'school', 'approved', 'date')
    search_fields = ('user', 'school', 'approved')
    list_filter = ('approved', 'date')


class CourseShiftsInline(admin.StackedInline):
    model = CourseShift
    can_delete = True
    verbose_name_plural = 'Shifts'

    def get_max_num(self, request, obj=None, **kwargs):
        return 3


class CourseAdmin(admin.ModelAdmin):
    # inlines = (CourseShiftsInline, )
    list_display = ('pk', 'type', 'get_school')
    list_display_links = ('type', )
    search_fields = ('type', )

    def get_school(self, obj):
        return obj.school.name

    get_school.short_description = 'School'


class CoursePeriodAdmin(admin.ModelAdmin):
    inlines = (CourseShiftsInline, )
    list_display = ('get_description', 'get_morning', 'get_afternoon', 'get_night')
    list_display_links = ('get_description', )
    search_fields = ('weeks', )
    # list_filter = ('course__description', )
    ordering = ('id', )

    def get_description(self, obj):
        if not obj.weeks:
            return obj.course.type.name
        return obj.course.type.name + ' - ' + str(obj.weeks) + ' weeks'

    get_description.short_description = 'Description'

    def get_morning(self, obj):
        return obj.get_total_price_morning_shift()

    get_morning.short_description = 'Morning Total Price'

    def get_afternoon(self, obj):
        return obj.get_total_price_afternoon_shift()

    get_afternoon.short_description = 'Afternoon Total Price'

    def get_night(self, obj):
        return obj.get_total_price_night_shift()

    get_night.short_description = 'Night Total Price'


class PromotionAdmin(admin.ModelAdmin):
    list_display = ('pk', 'course_period', 'start_date', 'end_date', 'value')
    search_fields = ('course_period', )
    list_filter = ('start_date', 'end_date')


class RatingAdmin(admin.ModelAdmin):
    list_display = ('school', 'user', 'type_of_rating', 'stars', 'comment')


class AlreadyStudiedHereAdmin(admin.ModelAdmin):
    list_display = ('user', 'school', 'data', 'comment')


class SchoolCertificateAdmin(admin.ModelAdmin):
    model = SchoolCertificate


class FacilitiesAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_icon_image')
    list_display_links = ('name', )
    search_fields = ('name', )

    def get_icon_image(self, obj):
        return True if obj.icon_text or obj.image else False

    get_icon_image.short_description = 'Possui icon/foto?'
    get_icon_image.boolean = True


admin.site.register(School, SchoolAdmin)
admin.site.register(ImageGalery)
admin.site.register(EventsOffered)
admin.site.register(Videos)
admin.site.register(Accommodation)
admin.site.register(Languages)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(CoursePeriod, CoursePeriodAdmin)
admin.site.register(Promotion, PromotionAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(AlreadyStudiedHere, AlreadyStudiedHereAdmin)
admin.site.register(SchoolCertificate, SchoolCertificateAdmin)
admin.site.register(CourseType)
admin.site.register(Facilities, FacilitiesAdmin)

# admin.site.register(CourseShifts)
