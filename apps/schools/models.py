from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from ..utils.rating_categories import RATINGS_CATEGORIES
from ..profile.models import Profile
from django.db.models import Avg
from ckeditor.fields import RichTextField
from django.utils.text import slugify

from apps.adresses.models import City

NO_IMAGE = 'http://school-stalker.herokuapp.com/static/assets/img/circle2.png'


class Accommodation(models.Model):

    TYPE_OF_ACOMMODATIONS = (
        ("school_house", "Student Acommodation"),
        ("home_stay", "Home Stay"),
        ("hostel", "Hostel")
    )

    is_beakfest_included = models.BooleanField(
        _(u"Is BreakFeast included?"),
        help_text=_(u"Is BreakFest Included?"),
        default=False
    )

    is_lunch_included = models.BooleanField(
        _(u"Is Lunch included?"),
        help_text=_(u"Is lunch Included?"),
        default=False
    )

    is_dinner_included = models.BooleanField(
        _(u"Is Dinner Included?"),
        help_text=_(u"Is dinner Included?"),
        default=False
    )

    is_tv_in_the_bedroom = models.BooleanField(
        _(u"Do you have a tv in the bedroom?"),
        help_text=_(u"Do you have a tv in the bedroom?"),
        default=False
    )

    is_wardrobe_in_the_bedroom = models.BooleanField(
        _("Do you have a wardrobe in the bedroom?"),
        help_text=_(u"Do you have a wardrobe in the bedroom?"),
        default=False
    )

    is_air_in_the_badroom = models.BooleanField(
        _("Do you have a air conditioning in the bedroom?"),
        help_text=_(u"Do you have a air conditioning in the bedroom?"),
        default=False
    )

    duration = models.CharField(
        _(u"Period of stay in"),
        help_text=_(u"Time of acomodation"),
        max_length=255
    )

    wifi_free = models.BooleanField(
        _(u"Do you have free wifi?"),
        help_text=_(u"Do you have free wifi?"),
        default=False
    )

    time_to_school = models.CharField(
        _(u"How much time it take from the school?"),
        max_length=255,
        help_text=_(u"How much time it take from the school?")
    )

    differentials = models.CharField(
        _(u"Tell Me about your differentials"),
        help_text=_(u"What is your accomodation's differential"),
        max_length=255
    )

    def __str__(self):
        return str(self.id)


class Languages(models.Model):
    description = models.CharField(max_length=20)
    initials = models.CharField(max_length=5)

    def __str__(self):
        return self.description + ' - ' + self.initials


class SchoolCertificate(models.Model):
    name = models.CharField(max_length=150, null=False, blank=False)
    description = models.CharField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to='certificate')
    url = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_image(self):
        return self.image.url if self.image else NO_IMAGE


class Facilities(models.Model):
    name = models.CharField(null=False, blank=False, max_length=100)
    icon_text = models.CharField(null=True, blank=True, max_length=30,
                                 help_text='Informe o nome do icone (https://materializecss.com/icons.html)')
    image = models.ImageField(null=True, blank=True, upload_to='facilities',
                              help_text='Caso não tenha um ícone no site, '
                                        'envie aqui a imagem. Lembre-se que'
                                        ' a imgem deve ser pequena, ter '
                                        'qualidade e seguir o padrão de design')

    def __str__(self):
        return self.name


class School(models.Model):
    """
        School model class.
        Set school information in database models
    """
    # short_phrase = models.TextField(null=True, blank=True)
    short_phrase = RichTextField(null=True, blank=True)

    name = models.CharField(
        _(u'School Name'),
        max_length=255,
        db_index=True
    )

    logo_image = models.ImageField(
        _(u"Upload o logo do seu computador"),
        upload_to='schools',
        blank=True
    )
    address = models.CharField(max_length=200,
        help_text=_(u"Informe o endereço da escola"),
        null=True, blank=True
    )
    neighborhood = models.CharField(max_length=200,
        help_text=_(u"Informe o bairro"),
        null=True, blank=True
    )

    city = models.ForeignKey(
        City, null=True, blank=True, verbose_name='Informe a cidade')

    youtube_channel_link = models.CharField(
        _(u"Youtube Channel"),
        help_text=_(u"Set the link for yout youtube channel or video"),
        max_length=255, null=True, blank=True
    )

    facebook_page = models.CharField(
        _(u"Facebook Page"),
        help_text=_(u"Facebook Page"),
        max_length=255, null=True, blank=True
    )

    twitter_page = models.CharField(
        _(u"Twitter Page"),
        help_text=_(u"Twitter Page"),
        max_length=500, null=True, blank=True
    )

    website = models.CharField(
        _(u"WebSite"),
        help_text=_(u"Your website"),
        max_length=255, null=True, blank=True
    )

    instagram = models.CharField(
        _(u"Instagram"),
        max_length=255,
        help_text=_(u"Instagram profile"), null=True, blank=True
    )

    admin_email = models.EmailField(
        _(u'Email'),
        max_length=100,
        unique=True,
        help_text=_(u"Email de contato da escola"), null=True, blank=True
    )

    phone = models.CharField(
        _(u'telefone'),
        max_length=18,
        null=True, blank=True,
        help_text=_(u"Ex.: +55 11 91001 0101")
    )

    internationa_phone = models.CharField(
        _(u'telefone'),
        max_length=18,
        null=True, blank=True,
        help_text=_(u"Ex.: +55 11 91001 0101")
    )

    registration_fee = models.DecimalField(
        _(u"Registration Fee Price"),
        max_digits=10,
        help_text=_(u"Registration Fee Price"),
        decimal_places=2,
        null=True,
        blank=True,
        default=0
    )

    workbook = models.DecimalField(
        _(u"Workbook Price"),
        max_digits=10,
        help_text=_(u"HWorkbook Price"),
        decimal_places=2,
        null=True,
        blank=True,
        default=0
    )

    refounds = models.DecimalField(
        _(u"Redounds"),
        max_digits=10,
        help_text=_(u"Refounds"),
        decimal_places=2,
        null=True,
        blank=True,
        default=0
    )

    low_season = models.CharField(
        _(u"Low season"),
        max_length=400,
        help_text=_(u"Low season"),
        null=True,
        blank=True
    )

    high_season = models.CharField(
        _(u"High season"),
        max_length=400,
        help_text=_(u"High season"),
        null=True,
        blank=True
    )

    work_time = models.TextField(
        verbose_name=_('Working time'),
        help_text=_('Inform the working time'), blank=True, null=True
    )

    coords = models.CharField(
        max_length=100, null=True, blank=True, verbose_name='Coordenadas')

    embeded_map = models.TextField(null=True, blank=True)

    accomodation = models.ForeignKey(
        Accommodation, null=True, blank=True)

    language = models.ForeignKey(Languages, null=True, blank=True)

    google_api_code = models.CharField(
        max_length=200, verbose_name='Código do Google Api', null=True,
        blank=True, unique=True)

    highlighted = models.BooleanField(verbose_name=_('Em destaque'),
                                      default=False,
                                      help_text='Se a escola estará em destaque no site.')
    slug = models.SlugField(max_length=255, unique=True, null=True, blank=False)

    show_budget = models.BooleanField(verbose_name=_('Apresentar orçamento no site'),
                                      default=False,
                                      help_text='Após cadastrar os cursos, '
                                                'periodos e valores da escola, '
                                                'marque essa opção para '
                                                'apresentar o botão de '
                                                'orçamento na listagem de '
                                                'escolas do site')

    certificates = models.ManyToManyField(SchoolCertificate, blank=True)
    facilities = models.ManyToManyField(Facilities, blank=True)
    favorite_users = models.ManyToManyField(Profile, related_name='favorite_schools', blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(School, self).save(*args, **kwargs)

    @property
    def events(self):
        return self.eventsoffered_set.get_queryset()

    @property
    def ratings(self):
        rating_total = self.rating_set.get_queryset().aggregate(
            Avg('stars')).get('stars__avg')
        rating_total = round(rating_total, 2) if rating_total else 1
        return rating_total

    @property
    def gallery(self):
        return self.gallery

    @property
    def videos(self):
        return self.videos_set.get_queryset()

    @property
    def ratings_count(self):
        return self.ratings_set.get_queryset().count()

    @property
    def comments(self):
        return self.comment_set.all()

    @property
    def full_rating(self):
        ratings = Rating.objects.filter(school=self.pk)
        res = {}

        for rating in ratings:
            if rating.type_of_rating not in res:
                res[rating.type_of_rating] = {}
                res[rating.type_of_rating]['sum'] = 0
                res[rating.type_of_rating]['qtd'] = 0
                res[rating.type_of_rating]['total'] = 0

            res[rating.type_of_rating]['sum'] += rating.stars
            res[rating.type_of_rating]['qtd'] += 1
            res[rating.type_of_rating]['total'] = \
                res[rating.type_of_rating]['sum'] / res[rating.type_of_rating]['qtd']

        return res

    def get_image(self):
        return self.logo_image.url if self.logo_image else NO_IMAGE

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('school-profile', kwargs={'pk': str(self.id)})

    def new_student(self, already_studied_here):
        if AlreadyStudiedHere.objects.filter(
                user=already_studied_here.user, school=self).exists():
            raise Exception('Student already enrolled in this school')
        return already_studied_here.save()

    def unroll_student(self, student):
        al = AlreadyStudiedHere.objects.get(school=self, user=student)
        return al.delete()

    def new_comment(self, student, comment):
        comment_obj = Comment(user=student, comment=comment)
        self.comment_set.add(comment_obj, bulk=False)

    def new_rating(self, student, rating):
        student_enroled = self.alreadystudiedhere_set.filter(
            user=student).exists()
        if student_enroled:
            rating.user = student
            rating.school = self
            rating.save()
        else:
            raise Exception('Student not enrolled in this School')

    def students_enrolled(self):
        return Profile.objects.filter(
            id__in=self.alreadystudiedhere_set.all().values_list('user'))

    def ratings_detail(self):
        return self.alreadystudiedhere_set.all()

    def get_full_address(self):
        if self.city:
            res = self.city.name
            if self.city.state and self.city.state.country:
                res += ', ' + self.city.state.country.name
            return res
        else:
            return ''


class ImageGalery(models.Model):
    image = models.ImageField(upload_to='galery', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    school = models.ForeignKey(School, related_name='gallery')
    description = models.CharField(max_length=100)

    def __str__(self):
        return self.image.name if self.image else self.school.name

    def get_image(self):
        return self.image.url if self.image else NO_IMAGE


class Videos(models.Model):
    description = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    school = models.ForeignKey(School)
    embeded_code = models.TextField(default='')


class EventsOffered(models.Model):
    DAYS_WEEK = (
        ('1', _('Domingo')),
        ('2', _('Segunda-feira')),
        ('3', _('Terça-feira')),
        ('4', _('Quarta-feira')),
        ('5', _('Quinta-feira')),
        ('6', _('Sexta')),
        ('7', _('Sábado')),
    )
    description = models.CharField(max_length=100)

    day = models.CharField(
        max_length=1,
        verbose_name=_('Dia da semana'),
        choices=DAYS_WEEK)

    price = models.DecimalField(
        max_digits=5, decimal_places=2)

    school = models.ForeignKey(School)

    @property
    def week_day(self):
        a = {'1': _('Domingo'),
             '2': _('Segunda-feira'),
             '3': _('Terça-feira'),
             '4': _('Quarta-feira'),
             '5': _('Quinta-feira'),
             '6': _('Sexta-feira'),
             '7': _('Sábado')
             }
        return a.get(str(self.day))

    def __str__(self):
        return self.description


class Rating(models.Model):
    school = models.ForeignKey(
        School,
    )
    user = models.ForeignKey(
        Profile,
    )
    type_of_rating = models.CharField(
        _(u"Ratings"),
        choices=RATINGS_CATEGORIES,
        max_length=15
    )
    stars = models.IntegerField(
        _(u"How many Stars Do you think this school diserve?"),
        validators=[MinValueValidator(0), MaxValueValidator(5)]
    )
    comment = models.CharField(
        _(u"Why this note?"),
        max_length=255,
        help_text=_(u"Why Would you like to do this rating?")
    )

    def __str__(self):
        return str(self.stars)

    class Meta:
        unique_together = ("school", "user", "type_of_rating")

    def save(self, *args, **kwargs):
        ash = AlreadyStudiedHere.objects\
            .filter(user=self.user, school=self.school).first()
        if ash:
            ash.update_average()
        return super(Rating, self).save(*args, **kwargs)


class CourseType(models.Model):
    name = models.CharField(blank=False, null=False, max_length=200)

    def __str__(self):
        return self.name


class Course(models.Model):
    CLASS_TYPE = (
        ('individual', 'Individual'),
        ('group', 'Group'),
    )

    description = models.TextField(null=True, blank=True)
    school = models.ForeignKey(School,
                               on_delete=models.CASCADE,
                               related_name='courses')

    type = models.ForeignKey(CourseType,
                             related_name='courses', null=True, blank=True)

    visa_needed = models.BooleanField(
        help_text='Visa is needed?'
    )

    classes_type = models.CharField(
        max_length=20, choices=CLASS_TYPE
    )

    def __str__(self):
        res = ''

        if self.school:
            res = self.school.name + ' - '

        if self.type:
            res += self.type.name

        return res


class CoursePeriod(models.Model):

    course = models.ForeignKey(Course,
                               on_delete=models.CASCADE,
                               related_name='periods')

    weeks = models.IntegerField(_(u'Total weeks'), blank=False, null=True)

    hours_week = models.DecimalField(
        _(u'Hours by week'), max_digits=6, decimal_places=1, blank=False, null=True)

    tax_book = models.DecimalField(max_digits=6,
                                   decimal_places=2,
                                   help_text='Price',
                                   null=True,
                                   blank=True,
                                   default=0)

    tax_registration = models.DecimalField(max_digits=6,
                                           decimal_places=2,
                                           help_text='Registration',
                                           null=True,
                                           blank=True,
                                           default=0)

    tax_insurance_gov = models.DecimalField(max_digits=6,
                                            decimal_places=2,
                                            help_text='Insurance GOV/GTA',
                                            null=True,
                                            blank=True,
                                            default=0)

    tax_ad_fees = models.DecimalField(max_digits=6,
                                      decimal_places=2,
                                      help_text='Ad. Fees',
                                      null=True,
                                      blank=True,
                                      default=0)

    tax_transfer = models.DecimalField(max_digits=6,
                                       decimal_places=2,
                                       help_text='Transfer',
                                       null=True,
                                       blank=True,
                                       default=0)

    tax_exam_fee = models.DecimalField(max_digits=6,
                                       decimal_places=2,
                                       help_text='Exam fee',
                                       null=True,
                                       blank=True,
                                       default=0)

    def __str__(self):
        return self.course.type.name + ' - ' + str(self.weeks) + ' weeks'

    def get_total_tax(self):
        return self.tax_book + \
               self.tax_registration + \
               self.tax_insurance_gov + \
               self.tax_ad_fees + \
               self.tax_transfer + \
               self.tax_exam_fee

    def get_total_price_morning_shift(self):
        price = self.shifts.filter(shift='morning').values_list('price', flat=True).first()
        if not price:
            return 0

        return price + self.get_total_tax()

    def get_total_price_afternoon_shift(self):
        price = self.shifts.filter(shift='afternoon').values_list('price', flat=True).first()
        if not price:
            return 0

        return price + self.get_total_tax()

    def get_total_price_night_shift(self):
        price = self.shifts.filter(shift='night').values_list('price', flat=True).first()
        if not price:
            return 0

        return price + self.get_total_tax()


class CourseShift(models.Model):
    SHIFTS = (
        ('morning', 'Morning'),
        ('afternoon', 'Afternoon'),
        ('night', 'Night'),
    )

    course_period = models.ForeignKey(CoursePeriod,
                                      on_delete=models.CASCADE,
                                      related_name='shifts')
    shift = models.CharField(max_length=20, choices=SHIFTS)
    price = models.DecimalField(max_digits=6,
                                decimal_places=2,
                                help_text='Price')

    def save(self, *args, **kwargs):
        count = CourseShift.objects\
            .filter(course_period=self.course_period, shift=self.shift)\
            .exclude(pk=self.pk).count()

        if count == 0:
            return super(CourseShift, self).save(*args, **kwargs)
        else:
            raise Exception('Shift already in use')

    def get_total(self):
        total = self.course_period.get_total_tax() + self.price
        if not total:
            return 0

        return total


class Promotion(models.Model):

    value = models.DecimalField(
        _(u'Value in percent (%)'), max_digits=4, decimal_places=2, blank=False, null=True)

    start_date = models.DateTimeField(auto_now_add=False)
    end_date = models.DateTimeField(auto_now_add=False)
    course_period = models.ForeignKey(CoursePeriod)


class Comment(models.Model):
    user = models.ForeignKey(Profile)
    school = models.ForeignKey(School)
    comment = models.TextField()
    approved = models.BooleanField(default=True)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-date',)

    def __str__(self):
        return self.user.user.first_name


class AlreadyStudiedHere(models.Model):
    user = models.ForeignKey(Profile)
    school = models.ForeignKey(School)
    data = models.DateField(auto_now=False, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    average = models.FloatField(null=True, blank=True)

    class Meta:
        unique_together = (("user", "school"),)

    def __str__(self):
        return self.user.user.first_name

    def ratings(self):
        return Rating.objects.filter(
            school=self.school, user_id=self.user.id)

    def update_average(self):
        sum = 0
        average = 0
        ratings = self.ratings()
        for rating in ratings:
            sum += rating.stars if rating.stars else 0
            average = round(sum / len(ratings), 2)

        self.average = average
        self.save()